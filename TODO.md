Things missing from the project

- Docker / Kubernets config
- Create protocol for formatters (format/1 function)
- Document each module with `@moduledoc` (currently set to `false`)
- Understand how to model `Protocol Buffers` responses properly (things under
  `/sportify_web/definitions/`)
- Better metric data (eg.: how long does it take to filter the queries)

