defmodule SportifyWeb.MixProject do
  use Mix.Project

  def project do
    [
      app: :sportify_web,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {SportifyWeb.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false},
      {:plug_cowboy, "~> 2.0"},
      {:jason, "~> 1.1"},
      {:exprotobuf, "~> 1.2.9"},
      {:sportify, path: "../sportify"}
    ]
  end
end
