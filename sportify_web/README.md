# SportifyWeb

`SportifyWeb` is the web layer for the `Sportify` application. It has very
little logic outside of HTTP handling. It responds to both `JSON` and `Protocol
Buffers.`

Endpoints:

- `GET /seasons`
  If `Accept = "application/(x-protobuf/protobuf/octet-stream)"` is set it will
  return all leagues and seasons in that format, otherwise it'll parse
  everything as JSON. 
  Always returns 200 OK.

- `GET /results`
  If `Accept = "application/(x-protobuf/protobuf/octet-stream)"` is set it will
  return all results in that format, otherwise it'll parse everything as JSON. Receives filters via query-string 
  Eg.: `GET /results?league=Bundesliga&home_team=Hamburg`
  Always returns 200 OK.

Any other endpoint will return a `404 Not Found`.
