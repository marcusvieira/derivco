defmodule SportifyWeb.OutputFormatter do
  @moduledoc false

  alias SportifyWeb.Definitions.{MatchResult, Season}

  def format(content, :json) do
    Jason.encode!(content)
  end

  def format([%{"home_team" => _} | _] = content, :protobuf) do
    results =
      content
      |> atomize_map()
      |> Enum.map(&MatchResult.Result.MatchResult.new(&1))

    %{results: results}
    |> MatchResult.Result.new()
    |> MatchResult.Result.encode()
  end

  def format([%{"league" => _, "season" => _} | _] = content, :protobuf) do
    seasons =
      content
      |> atomize_map()
      |> Enum.map(&Season.Result.Season.new(&1))

    %{seasons: seasons}
    |> Season.Result.new()
    |> Season.Result.encode()
  end

  defp atomize_map(map) do
    Enum.map(map, &Map.new(&1, fn {k, v} -> {String.to_atom(k), v} end))
  end
end
