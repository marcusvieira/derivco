defmodule SportifyWeb.Plugs.Accept do
  @moduledoc false

  use Plug.Builder

  plug(:accept)

  def accept(conn, _opts) do
    if has_protobuf_header(conn) do
      assign(conn, :respond_in, :protobuf)
    else
      assign(conn, :respond_in, :json)
    end
  end

  def has_protobuf_header(conn) do
    Enum.any?(get_req_header(conn, "accept"), fn h ->
      h in protobuf_headers()
    end)
  end

  defp protobuf_headers do
    [
      "application/x-protobuf",
      "application/protobuf",
      "application/octet-stream"
    ]
  end
end
