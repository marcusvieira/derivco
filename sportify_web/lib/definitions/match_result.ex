defmodule SportifyWeb.Definitions.MatchResult do
  @moduledoc false

  use Protobuf, """
    message Result {
      message MatchResult {
        string home_team = 1;
        string away_team = 2;
        string date = 3;
        string full_time_result = 4;
        string league = 5;
        string season = 6;
      }

      repeated MatchResult results = 1;
    }
  """
end
