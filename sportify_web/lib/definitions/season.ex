defmodule SportifyWeb.Definitions.Season do
  @moduledoc false

  use Protobuf, """
    message Result {
      message Season {
        string league = 1;
        string season = 2;
      }

      repeated Season seasons = 1;
    }
  """
end
