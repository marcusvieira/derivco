defmodule SportifyWeb do
  @moduledoc false

  @spec all_leagues_and_seasons() :: list(map())
  def all_leagues_and_seasons do
    Sportify.all_leagues_and_seasons()
  end

  @spec all_leagues_and_seasons() :: list(map())
  def filter(query) do
    Sportify.filter(query)
  end
end
