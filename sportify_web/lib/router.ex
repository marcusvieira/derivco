defmodule SportifyWeb.Router do
  use Plug.Router

  require Logger

  plug(:match)
  plug(Plug.Parsers, parsers: [:urlencoded, :multipart])
  plug(SportifyWeb.Plugs.Accept)
  plug(:dispatch)

  get "/seasons" do
    respond_in = conn.assigns.respond_in

    Logger.info(
      "SportifyWeb: handling /seasons request with format #{respond_in}"
    )

    result =
      SportifyWeb.all_leagues_and_seasons()
      |> SportifyWeb.OutputFormatter.format(respond_in)

    conn
    |> put_resp_header("content-type", "application/#{respond_in}")
    |> send_resp(200, result)
  end

  get "/results" do
    respond_in = conn.assigns.respond_in
    query_params = conn.query_params

    Logger.info(
      "SportifyWeb: handling /results request with format #{respond_in} and filters #{
        inspect(query_params)
      }"
    )

    result =
      query_params
      |> SportifyWeb.filter()
      |> SportifyWeb.OutputFormatter.format(respond_in)

    conn
    |> put_resp_header("content-type", "application/#{respond_in}")
    |> send_resp(200, result)
  end

  match _ do
    send_resp(conn, 404, "Invalid Request")
  end
end
