defmodule SportifyWeb.RouterTest do
  use ExUnit.Case
  use Plug.Test

  @opts SportifyWeb.Router.init([])

  describe "/seasons" do
    test "returns a list with all seasons and leagues" do
      conn = conn(:get, "/seasons")

      conn = SportifyWeb.Router.call(conn, @opts)
      body = Jason.decode!(conn.resp_body)

      assert conn.state == :sent
      assert conn.status == 200
      assert [%{"league" => "La Liga", "season" => "201617"} | _] = body
    end

    test "returns a binary response with all seasons and leagues" do
      conn =
        conn(:get, "/seasons")
        |> put_req_header("accept", "application/x-protobuf")

      conn = SportifyWeb.Router.call(conn, @opts)

      assert conn.state == :sent
      assert conn.status == 200
      assert is_binary(conn.resp_body)
    end
  end

  describe "/results" do
    test "returns a list with all results given a filter" do
      conn = conn(:get, "/results?league=La Liga")

      conn = SportifyWeb.Router.call(conn, @opts)
      body = Jason.decode!(conn.resp_body)

      assert conn.state == :sent
      assert conn.status == 200
      assert [%{"league" => "La Liga"} | _] = body
    end

    test "returns a binary response with all results given a filter" do
      conn =
        conn(:get, "/results?league=La Liga")
        |> put_req_header("accept", "application/x-protobuf")

      conn = SportifyWeb.Router.call(conn, @opts)

      assert conn.state == :sent
      assert conn.status == 200
      assert is_binary(conn.resp_body)
    end

    test "returns an empty list given an invalid filter" do
      conn = conn(:get, "/results?invalid=1234")

      conn = SportifyWeb.Router.call(conn, @opts)
      body = Jason.decode!(conn.resp_body)

      assert conn.state == :sent
      assert conn.status == 200
      assert body == []
    end
  end

  test "returns 404 when hitting an unexisting endpoint" do
    conn = conn(:get, "/invalid")

    conn = SportifyWeb.Router.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 404
  end
end
