defmodule SportifyWeb.Plugs.AcceptTest do
  use ExUnit.Case, async: true
  use Plug.Test

  alias SportifyWeb.Plugs.Accept

  describe "accept/2" do
    test "when accept header is protobuf related" do
      conn =
        conn(:get, "/seasons")
        |> put_req_header("accept", "application/protobuf")

      conn = Accept.accept(conn, [])

      assert conn.assigns == %{respond_in: :protobuf}
    end

    test "when accept header is empty" do
      conn = conn(:get, "/seasons")

      conn = Accept.accept(conn, [])

      assert conn.assigns == %{respond_in: :json}
    end
  end
end
