Hello! (:
This project consists of three applications:

`Store`
  Handles the parsing and storing of a CSV. It is accessed via a simple API
  where it returns all values in a list of maps.
  
`Sportify`
  The core of the project. It contains the CSV to be read, and exposes a number
  of functions through its API. 

`SportifyWeb`
  A simple web layer for `Sportify`. Responds with both JSON and ProtocolBuffers.
  It has two endpoints to serve the data from `Sportify`.

Each application has its own `README.md` file, which goes a bit deeper in
explaining its functionality and API.

To run this project all you need to do is install elixir (1.7.4) & erlang
(21.1.4), `cd` into `/sportify_web`, run `mix deps.get` and `mix run --no-halt` and everything
should work! The API can be accessed on `localhost:4001/` with the endpoints
specified on `sportify_web`'s documentation.

Each application also has its own `cover/` folder with HTML files showing the
test coverage for each module. You can also run `mix credo --strict` under each
app to check its static code analysis.
