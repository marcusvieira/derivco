# Store

The `Store` project is responsible for holding state inside an Agent given a
CSV.

After adding `Store` as a dependency to a project, it automatically starts an
Agent process, ready to be used.

Its API consists of the following functions:

- `populate/2`
  This function receives the path to a CSV file, and an atom for a module that
  has a `format/1` function, that formats the data for each row on the CSV.
  
- `all/1`
  This function receives a list with the keys you want to receive. It lists all
  the records with that specific key. Eg.: `Store.all([:one, :two])`

- `find_where/1`
  This function receives a list of maps, containing the filters by which the
  data will be filtered, only returning the record where all filters match. Eg.:
  `Store.find_where(%{one: "value"}, %{two: "value"})`

