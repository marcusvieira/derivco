defmodule FakeFormatter do
  def format(row) do
    row
    |> Map.to_list()
    |> Enum.reduce(%{}, fn {k, v}, acc ->
      Map.put(acc, k |> String.downcase() |> String.to_atom(), v)
    end)
  end
end

defmodule StoreTest do
  use ExUnit.Case

  setup do
    Store.Data.start_link()
    Store.populate("test/support/test.csv", FakeFormatter)

    :ok
  end

  describe "start_link/2" do
    test "starts a process with the formatted data" do
      pid = Process.whereis(Store.Data)

      assert Process.alive?(pid)
    end
  end

  describe "populate/2" do
    test "populates data with CSV and transforms it given a formatter" do
      result = Store.populate("test/support/test.csv", FakeFormatter)

      assert result == :ok
      assert [%{age: "25"}, %{age: "30"}] = Store.all([:age])
    end
  end

  describe "all/1" do
    test "returns all records for the given key" do
      result = Store.all([:age])

      assert [%{age: "25"}, %{age: "30"}] = result
    end
  end

  describe "find_where/1" do
    test "when searching by a single filter" do
      result = Store.find_where(%{age: "25"})

      assert [%{age: "25", country: "Brazil", name: "Marcus"}] = result
    end

    test "when searching by multiple filters" do
      result = Store.find_where(%{age: "30", country: "United State"})

      assert [] = result
    end

    test "when searching by invalid filters" do
      result = Store.find_where(%{non_existing: "value"})

      assert [] = result
    end
  end
end
