defmodule Store.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      %{
        id: Store.Data,
        start: {Store.Data, :start_link, []}
      }
    ]

    options = [name: Store.Supervisor, strategy: :one_for_one]
    Supervisor.start_link(children, options)
  end
end
