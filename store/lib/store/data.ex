defmodule Store.Data do
  @moduledoc false
  @agent_name __MODULE__

  use Agent
  require Logger

  def start_link do
    Agent.start_link(fn -> [] end, name: @agent_name)
  end

  def populate(path_to_file, formatter) do
    Agent.update(@agent_name, fn _state ->
      result =
        path_to_file
        |> read_from_csv()
        |> format_with(formatter)

      result
    end)
  end

  def all(fields) do
    @agent_name
    |> Agent.get(&find_all(&1, fields))
    |> Enum.uniq()
  end

  def find_where(filters) do
    Agent.get(@agent_name, &find_matches(&1, filters))
  end

  defp find_all(state, fields) do
    Enum.map(state, fn s ->
      Enum.reduce(fields, %{}, fn f, acc ->
        Map.put(acc, f, s[f])
      end)
    end)
  end

  defp find_matches(state, filters) do
    Enum.filter(state, fn s ->
      filters
      |> Map.to_list()
      |> Enum.all?(fn {f, v} ->
        s[f] == v
      end)
    end)
  end

  defp read_from_csv(path_to_file) do
    Logger.info("Store: reading CSV from #{path_to_file}")

    path_to_file
    |> File.stream!()
    |> CSV.decode!(headers: true)
    |> Enum.map(& &1)
  end

  defp format_with(data, formatter) do
    Enum.map(data, fn x ->
      formatter.format(x)
    end)
  end
end
