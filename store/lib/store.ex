defmodule Store do
  @moduledoc false

  defdelegate populate(file, formatter), to: Store.Data
  defdelegate all(fields), to: Store.Data
  defdelegate find_where(filters), to: Store.Data
end
