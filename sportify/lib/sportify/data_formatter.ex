defmodule Sportify.DataFormatter do
  @moduledoc false

  @keys [
    Div: "league",
    Season: "season",
    Date: "date",
    HomeTeam: "home_team",
    AwayTeam: "away_team",
    FTHG: "full_time_home_goals",
    FTAG: "full_time_away_goals",
    FTR: "full_time_result"
  ]

  @values [
    SP1: "La Liga",
    SP2: "Segunda Divisón",
    E0: "Premier League",
    D1: "Bundesliga",
    D: "Draw",
    H: "Home Victory",
    A: "Away Victory"
  ]

  def format(row) do
    row
    |> Map.to_list()
    |> transform()
  end

  defp transform(list) do
    Enum.reduce(list, %{}, fn {k, v}, acc ->
      case transform_key(k) do
        value when is_binary(value) ->
          Map.put(acc, value, transform_value(v))

        nil ->
          acc
      end
    end)
  end

  defp transform_key(key) do
    @keys[String.to_atom(key)]
  end

  defp transform_value(value) do
    @values[String.to_atom(value)] || value
  end
end
