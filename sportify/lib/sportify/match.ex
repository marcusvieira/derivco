defmodule Sportify.Match do
  @moduledoc false

  require Logger

  @spec all_leagues_and_seasons() :: list(map())
  def all_leagues_and_seasons do
    Logger.info("Sportify: fetching all leagues and seasons")

    Store.all(["league", "season"])
  end

  @spec filter(map()) :: list(map())
  def filter(query) do
    Logger.info("Sportify: filtering data with #{inspect(query)}")

    Store.find_where(query)
  end
end
