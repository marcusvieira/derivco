defmodule Sportify.Application do
  @moduledoc false

  use Application

  require Logger

  def start(_type, _args) do
    populate_store()

    children = []

    opts = [strategy: :one_for_one, name: Sportify.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp populate_store do
    Logger.info("Sportify: populating store")

    path_to_file = Path.expand("../../data.csv", __DIR__)
    Store.populate(path_to_file, Sportify.DataFormatter)
  end
end
