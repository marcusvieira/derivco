defmodule Sportify do
  @moduledoc false

  defdelegate all_leagues_and_seasons, to: Sportify.Match
  defdelegate filter(query), to: Sportify.Match
end
