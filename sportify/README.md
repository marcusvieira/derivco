# Sportify

`Sportify` is the core of the application. It knows how to handle and interpret
Soccer Matches data and serve it in an understandable format.

Depends on `Store` as a "database"

Its API consists of two functions:

- `all_leagues_and_seasons/0`
  Returns all league and season pairs available.

- `filter/1`
  Returns all Match data given the filters. Accepts a map with all keys and
  values to be filtered. Returns an empty list if no data is available.


This module can be easily extended to have more knowledge about the matches,
such as giving simple statistical data for a specific team, or league.
