defmodule Sportify.MixProject do
  use Mix.Project

  def project do
    [
      app: :sportify,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Sportify.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false},
      {:csv, "~> 2.0.0"},
      {:store, path: "../store"}
    ]
  end

  defp aliases do
    [
      test: ["test --no-start"]
    ]
  end
end
