defmodule Sportify.DataFormatterTest do
  use ExUnit.Case

  describe "format/1" do
    test "formats a given row" do
      row = %{"Div" => "SP1", "Season" => "201516", "HomeTeam" => "Barcelona"}

      result = Sportify.DataFormatter.format(row)

      assert %{
               "league" => "La Liga",
               "season" => "201516",
               "home_team" => "Barcelona"
             } = result
    end

    test "ignores unmapped keys" do
      row = %{
        "Div" => "SP1",
        "Season" => "201516",
        "HomeTeam" => "Barcelona",
        "OtherKey" => "2"
      }

      result = Sportify.DataFormatter.format(row)

      refute Map.has_key?(result, "other_key")
    end

    test "ignores empty keys" do
      row = %{
        "Div" => "SP1",
        "Season" => "201516",
        "HomeTeam" => "Barcelona",
        "" => "2"
      }

      result = Sportify.DataFormatter.format(row)

      refute Map.has_key?(result, "")
    end
  end
end
