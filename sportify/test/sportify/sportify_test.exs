defmodule SportifyTest do
  use ExUnit.Case

  setup_all do
    Store.Data.start_link()
    Store.Data.populate("test/support/sample.csv", Sportify.DataFormatter)

    :ok
  end

  describe "all_leagues_and_seasons/0" do
    test "returns all leagues and seasons" do
      result = Sportify.all_leagues_and_seasons()

      assert [%{"league" => "La Liga", "season" => "201617"}] = result
    end
  end

  describe "filter/1" do
    test "returns all match data given the filters" do
      result = Sportify.filter(%{"league" => "La Liga"})

      assert [%{"league" => "La Liga"} | _] = result
    end
  end
end
